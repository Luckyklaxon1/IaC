zone                = "ru-central1-b"
cloud_id                = "epdc9fj47sjbuabccsr5"
folder_id               = "b1ghun93qn0ik6onk96p"

resources_vm = [

	{
        cores           = 2
        memory          = 2
        instance_name   = "nginx"
        image           = "ubuntu-2004-lts"
	}
]

resources_net = {

    dns_zone            = "luckyklaxon"
    dns_type            = "A"
    dns_ttl             = 200
    subnet_name         = "sub_terraform"
    subnet_net          = ["192.168.17.0/24"]    
}
